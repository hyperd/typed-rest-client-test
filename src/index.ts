/* Playground to debug the Microsoft typed-rest-client behavior with AWX. USE IT AT YOUR OWN RISK */

import * as restm from 'typed-rest-client/RestClient';
import { IRequestOptions } from "typed-rest-client/Interfaces";
import * as data from './conf/url.json';


async function callPath(path: string) {

    const requestOptions: IRequestOptions = {
        ignoreSslError: true,
        allowRedirectDowngrade: true,
        allowRedirects: true,
        // maxRedirects: 10
    };

    let rest: restm.RestClient = new restm.RestClient('awx-test',
        data.base_url, null, requestOptions);

    let response: any;

    try {
        console.log("path: " + path)
        response = await rest.get(path);
    }
    catch (_e) {
        let e: Error = _e;
        console.log('err: ' + e)
    }

    return response
}

async function executeRequest() {

    let response: any;

    const paths = data.paths
    for (const path of paths) {
        response = await callPath(path)

        response != undefined && console.log(response);
    }

}

executeRequest();