# typed-rest-client-test

This simple code will help you debugging Microsoft's [**typed-rest-client**](https://github.com/microsoft/typed-rest-client/) library if you encountered the issue [#201](https://github.com/microsoft/typed-rest-client/issues/201) _Redirects using relative URLs are not resolved using base URL_

## Configure

Clone this repository.

```bash
git clone
```

Enter the dir typed-rest-client0-test and run:

```bash
npm install
```

According to the test you want to run once the setup is complete, modify the [url.json](./src/conf/URL.json).
Here to follow an example:

```JSON
{
    "base_url": "https://google.com",
    "paths": [
        "/home",
        "home/"
    ]
}
```

## Use it

Run `node ./src/index.js` from the project's root.
The code will test every relative path to the base URL you have defined in [url.json](./src/conf/URL.JSON), producing the **response** in return, or a proper debug message in case of glitches.

## Compile it

Make sure you have the latest LTS version of Node.Js.
Install `typescript`:

```bash
npm install -g typescript
```

Compile the code from the project's root as follows:

```bash
tsc ./src/index.ts  --resolveJsonModule
```
